﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TrueWheels.BEL;
using TrueWheels.DAL;
using TrueWheels.Web.Models;
using System.IO;

namespace TrueWheels.Web.Controllers
{
    public class UserDashBaordController : Controller
    {
        //
        // GET: /UserDashBaord/
        [HttpGet]
        public ActionResult Index()
        {
            if (Session["userDetail"] != null)
            {
                UserMaintenanceViewModel objUserMaintenance = new UserMaintenanceViewModel();
                //List<DashBaordViewModal> DVM = new List<DashBaordViewModal>;
                UserDetailsDAL objUserDAL = new UserDetailsDAL();
                List<DashBoardBEL> UserMenus = objUserDAL.GetMenuMapping((((TrueWheels.Web.Models.UserLoginDetailsViewModel)(Session["userDetail"])).User_ID).ToString());
                PersonalinfoBEL objPersonalInfo = objUserDAL.GetProfileInfo((((TrueWheels.Web.Models.UserLoginDetailsViewModel)(Session["userDetail"])).User_ID).ToString());
                objPersonalInfo.profilePic_Path = objPersonalInfo.User_Id + ".png";
                objPersonalInfo.Full_Name = objPersonalInfo.First_Name + " " + objPersonalInfo.Last_Name;
                ViewBag.DashBoardMenu = UserMenus;
                objUserMaintenance.Personalinfo = objPersonalInfo;

                if (string.IsNullOrEmpty(UserMenus[0].ErrorMessage))
                {
                    //  DVM = UserMenus;
                    return View(objUserMaintenance);
                }
                else
                {

                    ModelState.AddModelError("", UserMenus[0].ErrorMessage.ToString());
                }
            }
            return View();
        }

        [HttpPost]
        public ActionResult Index(PersonalinfoBEL profileInfo, HttpPostedFileBase profilePic)
         {
            if (ModelState.IsValid)
            {
                if (profilePic != null)
                {
                    var filename = profileInfo.User_Id + ".png";
                    var filePathOriginal = Server.MapPath("/Content/images/profilePicture");
                    string savedFileName = Path.Combine(filePathOriginal, filename);
                    string path = System.Configuration.ConfigurationManager.AppSettings["appPath"] + savedFileName;
                    System.IO.File.Delete(savedFileName);
                    profilePic.SaveAs(savedFileName);
                }


                //List<DashBaordViewModal> DVM = new List<DashBaordViewModal>;
                UserDetailsDAL objUserDAL = new UserDetailsDAL();
                List<DashBoardBEL> UserMenus = objUserDAL.GetMenuMapping((((TrueWheels.Web.Models.UserLoginDetailsViewModel)(Session["userDetail"])).User_ID).ToString());
                if (profileInfo.NewPassword == null)
                {
                    profileInfo.NewPassword = profileInfo.Password;
                }

                UserMaintenanceViewModel objUserMaintenance = new UserMaintenanceViewModel();

                objUserMaintenance.Personalinfo = objUserDAL.UpdateProfileInfo(profileInfo);
                objUserMaintenance.Personalinfo.profilePic_Path = objUserMaintenance.Personalinfo.User_Id + ".png";
                objUserMaintenance.Personalinfo.Full_Name = objUserMaintenance.Personalinfo.First_Name + " " + objUserMaintenance.Personalinfo.Last_Name;
                ViewBag.DashBoardMenu = UserMenus;

                if (string.IsNullOrEmpty(UserMenus[0].ErrorMessage))
                {
                    //  DVM = UserMenus;
                    return View(objUserMaintenance);
                }
                else
                {

                    ModelState.AddModelError("", UserMenus[0].ErrorMessage.ToString());
                    return View(objUserMaintenance);
                }
            }
            else
            {
                UserDetailsDAL objUserDAL = new UserDetailsDAL();
                List<DashBoardBEL> UserMenus = objUserDAL.GetMenuMapping((((TrueWheels.Web.Models.UserLoginDetailsViewModel)(Session["userDetail"])).User_ID).ToString());
                ViewBag.DashBoardMenu = UserMenus;
                UserMaintenanceViewModel objUserMaintenance = new UserMaintenanceViewModel();
                objUserMaintenance.Personalinfo = profileInfo;
                return View(objUserMaintenance);
            }
        }

        //public ActionResult Index(string userid)
        //{


        //    return View();
        //}

        //
        // GET: /UserDashBaord/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /UserDashBaord/Create
        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /UserDashBaord/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /UserDashBaord/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /UserDashBaord/Edit/5
        [HttpPost]
        public ActionResult EditPersonalInfo(UserMaintenanceViewModel objUserMaintenance)
        {


            return RedirectToAction("Index");
        }

        //
        // GET: /UserDashBaord/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /UserDashBaord/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        [HttpGet]
        public ActionResult FollowUs()
        {
            return PartialView("_FollowUs");
        }

        [HttpGet]
        public ActionResult InviteFriends()
        {
            return PartialView("_InviteFriends");
        }

        [HttpGet]
        public ActionResult Inbox()
        {
            UserDetailsDAL objUserDAL = new UserDetailsDAL();
            List<UserInboxBEL> ListUserNotifications = objUserDAL.GetUserInbox((((TrueWheels.Web.Models.UserLoginDetailsViewModel)(Session["userDetail"])).User_ID).ToString());

            return PartialView("_Inbox", ListUserNotifications);
        }

        public ActionResult DeleteNotification(string id)
        {
            if (Session["userDetail"] != null)
            {
                UserDetailsDAL objUserDAL = new UserDetailsDAL();
                UserInboxBEL UserNotification = objUserDAL.DeleteUserNotification((((TrueWheels.Web.Models.UserLoginDetailsViewModel)(Session["userDetail"])).User_ID).ToString(), id);
                if (UserNotification.Success)
                {
                    TempData["SaveSuccess"] = "Deleted Successfully";
                }
                else
                {
                    TempData["SaveSuccess"] = "An error occured";
                }
                List<UserInboxBEL> ListUserNotifications = objUserDAL.GetUserInbox((((TrueWheels.Web.Models.UserLoginDetailsViewModel)(Session["userDetail"])).User_ID).ToString());

                return PartialView("_Inbox", ListUserNotifications);
            }
            else
            {
                return PartialView("_Inbox");
            }

        }


        [HttpGet]
        public ActionResult ViewProfile()
        {
            if (Session["userDetail"] != null)
            {
                UserMaintenanceViewModel objUserMaintenance = new UserMaintenanceViewModel();
                //List<DashBaordViewModal> DVM = new List<DashBaordViewModal>;
                UserDetailsDAL objUserDAL = new UserDetailsDAL();
                List<DashBoardBEL> UserMenus = objUserDAL.GetMenuMapping((((TrueWheels.Web.Models.UserLoginDetailsViewModel)(Session["userDetail"])).User_ID).ToString());
                PersonalinfoBEL objPersonalInfo = objUserDAL.GetProfileInfo((((TrueWheels.Web.Models.UserLoginDetailsViewModel)(Session["userDetail"])).User_ID).ToString());
                objPersonalInfo.profilePic_Path = objPersonalInfo.User_Id + ".png";
                objPersonalInfo.Full_Name = objPersonalInfo.First_Name + " " + objPersonalInfo.Last_Name;
                ViewBag.DashBoardMenu = UserMenus;
                objUserMaintenance.Personalinfo = objPersonalInfo;

                if (string.IsNullOrEmpty(UserMenus[0].ErrorMessage))
                {
                    return PartialView("_Personalinfo", objPersonalInfo);
                }
                else
                {

                    ModelState.AddModelError("", UserMenus[0].ErrorMessage.ToString());
                }
            }

            return PartialView("_Personalinfo");
        }
    }
}
