﻿using Excel;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TrueWheels.BEL;
using TrueWheels.DAL;
using TrueWheels.Web.Models;

namespace TrueWheels.Web.Controllers
{
    public class RentSpaceController : Controller
    {
        //
        // GET: /RentSpace/
        public ActionResult CreateSpace()
        {
            return View();
        }

        public ActionResult UploadSpace(HttpPostedFileBase upload)
        {
            if (ModelState.IsValid)
            {

                if (upload != null && upload.ContentLength > 0)
                {
                    // ExcelDataReader works with the binary Excel file, so it needs a FileStream
                    // to get started. This is how we avoid dependencies on ACE or Interop:
                    Stream stream = upload.InputStream;

                    // We return the interface, so that
                    IExcelDataReader reader = null;


                    if (upload.FileName.EndsWith(".xls"))
                    {
                        reader = ExcelReaderFactory.CreateBinaryReader(stream);
                    }
                    else if (upload.FileName.EndsWith(".xlsx"))
                    {
                        reader = ExcelReaderFactory.CreateOpenXmlReader(stream);
                    }
                    else
                    {
                        ModelState.AddModelError("File", "This file format is not supported");
                        return View();
                    }

                    reader.IsFirstRowAsColumnNames = true;
                    //reader.
                    DataSet result = reader.AsDataSet();
                    reader.Close();
                    // start
                    DataColumn column = new DataColumn();
                    column.ColumnName = "Registered";
                    result.Tables[0].Columns.Add(column);
                    column = new DataColumn();
                    column.ColumnName = "ErrorMessage";
                    result.Tables[0].Columns.Add(column);
                    ParkingAreaDAL Parkingdal = new ParkingAreaDAL();
                    RegisterParkingAreaBEL ParkingBEL = new RegisterParkingAreaBEL();
                    RegsiterParkingAreaResult ParkingResult = new RegsiterParkingAreaResult();
                    for (int i=0;i<=result.Tables[0].Rows.Count-1;i++)
                    {
                        ParkingResult.ErrorMessage = "";
                        if (result.Tables[0].Rows[i]["owner_id"] != null && Convert.ToString(result.Tables[0].Rows[i]["owner_id"]) != "")
                            ParkingBEL.owner_id = Convert.ToInt32(result.Tables[0].Rows[i]["owner_id"]);
                        else
                            ParkingResult.ErrorMessage = "owner id can not be blank. ";

                        if (result.Tables[0].Rows[i]["Space_type"] != null && Convert.ToString(result.Tables[0].Rows[i]["Space_type"]) != "")
                           ParkingBEL.Space_type = Convert.ToString(result.Tables[0].Rows[i]["Space_type"]);
                        else
                            ParkingResult.ErrorMessage += " Space_type can not be blank. ";

                        if (result.Tables[0].Rows[i]["Property_type"] != null && Convert.ToString(result.Tables[0].Rows[i]["Property_type"]) != "")
                           ParkingBEL.Property_type = Convert.ToString(result.Tables[0].Rows[i]["Property_type"]);
                        else
                            ParkingResult.ErrorMessage += " Property_type can not be blank. ";

                        if (result.Tables[0].Rows[i]["No_of_space"] != null && Convert.ToString(result.Tables[0].Rows[i]["No_of_space"]) != "" )
                        ParkingBEL.No_of_space = Convert.ToString(result.Tables[0].Rows[i]["No_of_space"]);
                        else
                            ParkingResult.ErrorMessage += " No_of_space can not be blank. ";

                        if (result.Tables[0].Rows[i]["VechileType"] != null && Convert.ToString(result.Tables[0].Rows[i]["VechileType"]) != "")
                        ParkingBEL.VechileType = Convert.ToString(result.Tables[0].Rows[i]["VechileType"]);
                        else
                            ParkingResult.ErrorMessage += " VechileType can not be blank. ";

                        if (result.Tables[0].Rows[i]["PropertyVerifiedStatus"] != null && Convert.ToString(result.Tables[0].Rows[i]["PropertyVerifiedStatus"]) != "")
                        ParkingBEL.PropertyVerifiedStatus = Convert.ToString(result.Tables[0].Rows[i]["PropertyVerifiedStatus"]);
                        else
                            ParkingResult.ErrorMessage += " PropertyVerifiedStatus can not be blank. ";

                        ParkingBEL.FacilityGroupId = Convert.ToString(result.Tables[0].Rows[i]["FacilityGroupId"]);

                        if (result.Tables[0].Rows[i]["PropertyAddress"] != null && Convert.ToString(result.Tables[0].Rows[i]["PropertyAddress"]) != "")
                        ParkingBEL.PropertyAddress = Convert.ToString(result.Tables[0].Rows[i]["PropertyAddress"]);
                        else
                            ParkingResult.ErrorMessage += " No_of_space can not be blank. ";

                        if (result.Tables[0].Rows[i]["PropertyPinCode"] != null && Convert.ToString(result.Tables[0].Rows[i]["PropertyPinCode"]) != "")
                        ParkingBEL.PropertyPinCode = Convert.ToInt32(result.Tables[0].Rows[i]["PropertyPinCode"]);
                        else
                            ParkingResult.ErrorMessage += " PropertyPinCode can not be blank. ";

                        ParkingBEL.PropertyZone = Convert.ToString(result.Tables[0].Rows[i]["PropertyZone"]);

                        ParkingBEL.PropertyLandMark = Convert.ToString(result.Tables[0].Rows[i]["PropertyLandMark"]);

                        ParkingBEL.OwnerComments = Convert.ToString(result.Tables[0].Rows[i]["OwnerComments"]);

                        if (result.Tables[0].Rows[i]["lattitude"] != null && Convert.ToString(result.Tables[0].Rows[i]["lattitude"]) != "")
                        ParkingBEL.lattitude = Convert.ToString(result.Tables[0].Rows[i]["lattitude"]);
                        else
                            ParkingResult.ErrorMessage += " lattitude can not be blank. ";

                        if (result.Tables[0].Rows[i]["longitude"] != null && Convert.ToString(result.Tables[0].Rows[i]["longitude"]) != "")
                        ParkingBEL.longitude = Convert.ToString(result.Tables[0].Rows[i]["longitude"]);
                        else
                            ParkingResult.ErrorMessage += " longitude can not be blank. ";
                        if (ParkingResult.ErrorMessage == "")
                        {
                            ParkingResult = Parkingdal.RegisterParkingArea(ParkingBEL);

                            if (ParkingResult.ErrorMessage == "" && ParkingResult.Parking_Id != 0)
                            {
                                result.Tables[0].Rows[i]["Registered"] = "True";

                            }
                            else
                            {
                                result.Tables[0].Rows[i]["Registered"] = "False";
                                result.Tables[0].Rows[i]["ErrorMessage"] = ParkingResult.ErrorMessage;
                            }
                        }
                        else
                        {
                            result.Tables[0].Rows[i]["Registered"] = "False";
                            result.Tables[0].Rows[i]["ErrorMessage"] = ParkingResult.ErrorMessage;
                        }
                    }
                    // end
                    return View(result.Tables[0]);
                }
                else
                {
                    ModelState.AddModelError("File", "Please Upload Your file");
                }
            }
            return View();
        }
    }
}