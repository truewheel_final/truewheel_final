﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TrueWheels.Web.Models;
using TrueWheels.BEL;
using TrueWheels.DAL;
//using TrueWheels.DEL;
using System.Web.Script.Serialization;
using Newtonsoft.Json;

namespace TrueWheels.Web.Controllers
{
    public class ParkingAreaController : Controller
    {
        //
        // GET: /ParkingArea/
        
        public ActionResult Index(ParkingAreaViewModel vm)
        {
            ParkingAreaViewModel parkingViewMoel = TempData["ParkingAreaViewModel"] as ParkingAreaViewModel;
            ParkingAreaBEL parking = new ParkingAreaBEL();
            parking.Distance = vm.Distance;
            parking.FromDateTime = vm.FromDateTime;
            parking.Main_Latitude = vm.Main_Latitude;
            parking.Main_Longitude = vm.Main_Longitude;
            parking.ParkingClass = vm.ParkingClass;
            parking.ToDateTime = vm.ToDateTime;
            parking.OrderBy = vm.OrderBy;
            
            ParkingAreaDAL parkingDal = new ParkingAreaDAL();
            List<AvailableParkingAreaResult> availableParkingList = parkingDal.GetAvailableParking(parking);
            JavaScriptSerializer js = new JavaScriptSerializer();
            var str = js.Serialize(availableParkingList);
            ViewBag.availableParkingList = str;
            ViewBag.Lat = availableParkingList[0].lattitude;
            ViewBag.Lng = availableParkingList[0].longitude;
            return View(availableParkingList);
        }

        public ActionResult LocateParkingArea() 
          {
              if (Session["userDetail"] != null)
              {
                  return View(); 
              }
              else
              {
                  if (Request["address"] != null)
                      TempData["RequestedUrl"] = "ParkingArea/LocateParkingArea/?lat=" + Request["lat"].ToString() + "&long=" + Request["long"].ToString() + "&address=" + Request["address"].ToString() + "&parkId=" + Request["parkId"].ToString();
                  else
                      TempData["RequestedUrl"] = "ParkingArea/LocateParkingArea/?lat=" + Request["lat"].ToString() + "&long=" + Request["long"].ToString();
                  return RedirectToAction("Login", "TrueWheelsUser");

              }
        }

        [HttpPost]
        public ActionResult AvailableParkingList(string para, string viewModel)
        {
            List<AvailableParkingAreaResult> entity = JsonConvert.DeserializeObject<List<AvailableParkingAreaResult>>(viewModel);
            var result = new List<AvailableParkingAreaResult>();
            if (para == "Distance")
                result = entity.OrderBy(x => x.Distance).ToList();
            else if (para == "Rating")
                result = entity.OrderBy(x => x.Rating).ToList();
            else
                result = entity.OrderBy(x => x.BasicCharge).ToList();
            return PartialView("AvailableParking", result);
        }

        public JsonResult GetParkingdetail(string park_Id)
        {
            ParkingAreaDAL parkingDal = new ParkingAreaDAL();
            var getParkingDetails = parkingDal.GetParkingDetails(park_Id);
            return Json(getParkingDetails, JsonRequestBehavior.AllowGet);
        }

	}
}