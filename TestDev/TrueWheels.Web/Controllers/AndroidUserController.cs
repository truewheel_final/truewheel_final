﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TrueWheels.BEL;
using TrueWheels.DAL;

namespace TrueWheels.Web.Controllers
{
    public class AndroidUserController : ApiController
    {
        private static string className = "AndroidUserController";
        BETrueWheelsUser[] users = new BETrueWheelsUser[] 
        { 
            new BETrueWheelsUser { ID = 1, FullName = "Neeraj", UserName = "neeraj@truewheels.com", Gender = "Male" }, 
            new BETrueWheelsUser { ID = 2, FullName = "Dileep", UserName = "dileep@truewheels.com", Gender = "Male" }, 
            new BETrueWheelsUser { ID = 3, FullName = "Dinesh", UserName = "dinesh@truewheels.com",  Gender = "Male" } 
        };

        public IEnumerable<BETrueWheelsUser> GetAllProducts()
        {
           
                return users;
            
        }

        public IHttpActionResult GetProduct(int id)
        {
           
                var product = users.FirstOrDefault((p) => p.ID == id);
                if (product == null)
                {
                    return NotFound();
                }
                return Ok(product);
            
        }
    }
}
