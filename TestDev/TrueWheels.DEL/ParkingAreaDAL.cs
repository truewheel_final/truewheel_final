﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrueWheels.BEL;
using TrueWheels.DAL;
using Microsoft.ApplicationBlocks.Data;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.SessionState;
using System.Reflection;

namespace TrueWheels.DAL
{
    public class ParkingAreaDAL
    {
        private string connectionString = string.Empty;
        private static string className = "ParkingAreaDAL";
        public ParkingAreaDAL()
        {
            connectionString = ConfigurationManager.ConnectionStrings["conStr"].ToString();
            // later a different project will be build where  encrypted connection will maintained.
        }
        Transaction transaction = new Transaction();

        #region  Get available parking
        public List<AvailableParkingAreaResult> GetAvailableParking(ParkingAreaBEL ParkingBEL)
        {
            List<AvailableParkingAreaResult> AvailableParkings = new List<AvailableParkingAreaResult>();
            try
            {

                DataSet ds = null;

                SqlParameter[] SqlParms = new SqlParameter[8];

                SqlParms[0] = new SqlParameter("@Latitude", SqlDbType.Decimal, 10);
                SqlParms[0].Direction = ParameterDirection.Input;
                SqlParms[0].Value = ParkingBEL.Main_Latitude;

                SqlParms[1] = new SqlParameter("@Longitude", SqlDbType.Decimal, 10);
                SqlParms[1].Direction = ParameterDirection.Input;
                SqlParms[1].Value = ParkingBEL.Main_Longitude;

                SqlParms[2] = new SqlParameter("@distance", SqlDbType.Int);
                SqlParms[2].Direction = ParameterDirection.Input;
                SqlParms[2].Value = ParkingBEL.Distance;

                SqlParms[3] = new SqlParameter("@FromDateTime", SqlDbType.VarChar, 19);
                SqlParms[3].Direction = ParameterDirection.Input;
                SqlParms[3].Value = ParkingBEL.FromDateTime;

                SqlParms[4] = new SqlParameter("@ToDateTime", SqlDbType.VarChar, 19);
                SqlParms[4].Direction = ParameterDirection.Input;
                SqlParms[4].Value = ParkingBEL.ToDateTime;

                SqlParms[5] = new SqlParameter("@ParkingClass", SqlDbType.VarChar, 19);
                SqlParms[5].Direction = ParameterDirection.Input;
                SqlParms[5].Value = ParkingBEL.ParkingClass;

                SqlParms[6] = new SqlParameter("@OrderBy", SqlDbType.VarChar, 40);
                SqlParms[6].Direction = ParameterDirection.Input;
                SqlParms[6].Value = ParkingBEL.OrderBy;

                SqlParms[7] = new SqlParameter("@Error", SqlDbType.VarChar, 150);
                SqlParms[7].Direction = ParameterDirection.Output;

                //SqlParms[7] = new SqlParameter("@UserId", SqlDbType.NVarChar, 20);
                //SqlParms[7].Direction = ParameterDirection.Output;



                ds = SqlHelper.ExecuteDataset(connectionString, CommandType.StoredProcedure, "SP_GetAvaiolableParking", SqlParms);
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0 && Convert.ToString(SqlParms[7].Value) != "0")
                    {
                        AvailableParkings = (from DataRow parkingrow in ds.Tables[0].Rows
                                             select new AvailableParkingAreaResult
                                             {

                                                 Parking_id = Convert.ToString(parkingrow["Parking_id"]),

                                                 parking_address = (parkingrow["parking_address"] == null ? "" : Convert.ToString(parkingrow["parking_address"])),

                                                 street = (parkingrow["street"] == null ? "" : Convert.ToString(parkingrow["street"])),

                                                 city = (parkingrow["city"] == null ? "" : Convert.ToString(parkingrow["city"])),

                                                 state = (parkingrow["state"] == null ? "" : Convert.ToString(parkingrow["state"])),

                                                 lattitude = (parkingrow["lattitude"] == null ? "" : Convert.ToString(parkingrow["lattitude"])),

                                                 longitude = (parkingrow["longitude"] == null ? "" : Convert.ToString(parkingrow["longitude"])),

                                                 GeoLoc = (parkingrow["GeoLoc"] == null ? "" : Convert.ToString(parkingrow["GeoLoc"])),

                                                 DateTimeTo = (parkingrow["DateTimeTo"] == null ? "" : Convert.ToString(parkingrow["DateTimeTo"])),

                                                 DateTimeFrom = (parkingrow["DateTimeFrom"] == null ? "" : Convert.ToString(parkingrow["DateTimeFrom"])),

                                                 No_Of_Space_Avaiable = (parkingrow["No_Of_Space_Avaiable"] == null ? "" : Convert.ToString(parkingrow["No_Of_Space_Avaiable"])),

                                                 Detail_ID = (parkingrow["Detail_ID"] == null ? "" : Convert.ToString(parkingrow["Detail_ID"])),

                                                 BasicCharge = (parkingrow["BasicCharge"] == null ? 0 : Convert.ToInt32(parkingrow["BasicCharge"])),

                                                 ParkingClass = (parkingrow["ParkingClass"] == null ? "" : Convert.ToString(parkingrow["ParkingClass"])),

                                                 Rating = (parkingrow["ParkingRating"] == System.DBNull.Value ? 0 : Convert.ToInt32(parkingrow["ParkingRating"])),

                                                 Distance = (parkingrow["Distance"] == null ? "" : Convert.ToString(parkingrow["Distance"])),

                                                 Facilities = (parkingrow["FacilityGroupId"] == null ? "" : Convert.ToString(parkingrow["FacilityGroupId"])),
                                                 SpaceType = (parkingrow["Space_Type"] == null ? "" : Convert.ToString(parkingrow["Space_Type"])),
                                                 PropertyType = (parkingrow["Property_Type"] == null ? "" : Convert.ToString(parkingrow["Property_Type"])),
                                                 Success = true,
                                             }
                                        ).ToList<AvailableParkingAreaResult>();

                        return AvailableParkings;

                    }
                }
                else
                {
                    AvailableParkings.Add(new AvailableParkingAreaResult() { Success = false, ErrorMessage = Convert.ToString(SqlParms[7].Value) });
                    return AvailableParkings;
                }

            }
            catch (Exception ex)
            {
                AvailableParkings.Add(new AvailableParkingAreaResult() { Success = false, ErrorMessage = Convert.ToString(ex.Message.ToString()) });
                ErrorLog.Log(this.GetType().Name, MethodBase.GetCurrentMethod().Name, ex.Message.ToString(), HttpContext.Current.Session["userPhoneNo/Email"] != null ? HttpContext.Current.Session["userPhoneNo/Email"].ToString() : "");
                return AvailableParkings;
            }

            return AvailableParkings;
        }
        #endregion

        #region  Register parking area
        public RegsiterParkingAreaResult RegisterParkingArea(RegisterParkingAreaBEL ParkingBEL)
        {
            RegsiterParkingAreaResult ParkingDetail = new RegsiterParkingAreaResult();
            try
            {

                DataSet ds = null;

                SqlParameter[] SqlParms = new SqlParameter[17];

                SqlParms[1] = new SqlParameter("@owner_id", SqlDbType.Int);
                SqlParms[1].Direction = ParameterDirection.Input;
                SqlParms[1].Value = ParkingBEL.owner_id;

                SqlParms[2] = new SqlParameter("@Space_type", SqlDbType.VarChar, 10);
                SqlParms[2].Direction = ParameterDirection.Input;
                SqlParms[2].Value = ParkingBEL.Space_type;

                SqlParms[3] = new SqlParameter("@Property_type", SqlDbType.VarChar, 10);
                SqlParms[3].Direction = ParameterDirection.Input;
                SqlParms[3].Value = ParkingBEL.Property_type;

                SqlParms[4] = new SqlParameter("@No_of_space", SqlDbType.Int);
                SqlParms[4].Direction = ParameterDirection.Input;
                SqlParms[4].Value = ParkingBEL.No_of_space;

                SqlParms[5] = new SqlParameter("@VechileType", SqlDbType.VarChar, 10);
                SqlParms[5].Direction = ParameterDirection.Input;
                SqlParms[5].Value = ParkingBEL.VechileType;

                SqlParms[6] = new SqlParameter("@PropertyVerifiedStatus", SqlDbType.Char, 1);
                SqlParms[6].Direction = ParameterDirection.Input;
                SqlParms[6].Value = ParkingBEL.PropertyVerifiedStatus;

                SqlParms[7] = new SqlParameter("@FacilityGroupId", SqlDbType.VarChar, 5);
                SqlParms[7].Direction = ParameterDirection.Input;
                SqlParms[7].Value = ParkingBEL.FacilityGroupId;

                SqlParms[8] = new SqlParameter("@PropertyAddress", SqlDbType.VarChar, 200);
                SqlParms[8].Direction = ParameterDirection.Input;
                SqlParms[8].Value = ParkingBEL.PropertyAddress;

                SqlParms[9] = new SqlParameter("@PropertyPinCode", SqlDbType.Int);
                SqlParms[9].Direction = ParameterDirection.Input;
                SqlParms[9].Value = ParkingBEL.PropertyPinCode;

                SqlParms[10] = new SqlParameter("@PropertyZone", SqlDbType.VarChar, 100);
                SqlParms[10].Direction = ParameterDirection.Input;
                SqlParms[10].Value = ParkingBEL.PropertyZone;

                SqlParms[11] = new SqlParameter("@PropertyLandMark", SqlDbType.VarChar, 100);
                SqlParms[11].Direction = ParameterDirection.Input;
                SqlParms[11].Value = ParkingBEL.PropertyLandMark;

                SqlParms[12] = new SqlParameter("@OwnerComments", SqlDbType.VarChar, 19);
                SqlParms[12].Direction = ParameterDirection.Input;
                SqlParms[12].Value = ParkingBEL.OwnerComments;

                SqlParms[13] = new SqlParameter("@lattitude", SqlDbType.Decimal, 19);
                SqlParms[13].Direction = ParameterDirection.Input;
                SqlParms[13].Value = ParkingBEL.lattitude;

                SqlParms[14] = new SqlParameter("@longitude", SqlDbType.Decimal, 19);
                SqlParms[14].Direction = ParameterDirection.Input;
                SqlParms[14].Value = ParkingBEL.longitude;

                SqlParms[15] = new SqlParameter("@Error", SqlDbType.VarChar, 300);
                SqlParms[15].Direction = ParameterDirection.Output;

                SqlParms[16] = new SqlParameter("@Parking_id", SqlDbType.NVarChar, 150);
                SqlParms[16].Direction = ParameterDirection.Output;





                SqlHelper.ExecuteNonQuery(connectionString, CommandType.StoredProcedure, "Insert_RegParkingArea", SqlParms);

                if (Convert.ToString(SqlParms[16].Value) != "0" && Convert.ToString(SqlParms[15].Value) == "")
                {
                    ParkingDetail.Parking_Id = Convert.ToInt32(SqlParms[16].Value);
                    ParkingDetail.Success = true;
                    ParkingDetail.ErrorMessage = "";

                    return ParkingDetail;

                }
                else
                {
                    ParkingDetail.Parking_Id = 0;
                    ParkingDetail.Success = false;
                    ParkingDetail.ErrorMessage = Convert.ToString(SqlParms[15].Value);
                    return ParkingDetail;
                }


            }
            catch (Exception ex)
            {
                ParkingDetail.Parking_Id = 0;
                ParkingDetail.Success = false;
                ParkingDetail.ErrorMessage = ex.Message.ToString();
                ErrorLog.Log(this.GetType().Name, MethodBase.GetCurrentMethod().Name, ex.Message.ToString(), HttpContext.Current.Session["userPhoneNo/Email"] != null ? HttpContext.Current.Session["userPhoneNo/Email"].ToString() : "");
                return ParkingDetail;
            }

            return ParkingDetail;
        }
        #endregion

        #region  Get All Location for search
        public List<string> GetAllLocation(string Term)
        {
            List<string> Locations = new List<string>();
            try
            {

                DataSet ds = null;

                SqlParameter[] SqlParms = new SqlParameter[2];

                SqlParms[0] = new SqlParameter("@LocationTerm", SqlDbType.VarChar, 100);
                SqlParms[0].Direction = ParameterDirection.Input;
                SqlParms[0].Value = Term;

                SqlParms[1] = new SqlParameter("@Error", SqlDbType.NVarChar, 150);
                SqlParms[1].Direction = ParameterDirection.Output;


                ds = SqlHelper.ExecuteDataset(connectionString, CommandType.StoredProcedure, "SP_GetAllLocationName", SqlParms);

                if (ds.Tables[0].Rows.Count > 0 && Convert.ToString(SqlParms[1].Value) != "0")
                {
                    return Locations =
                        (from DataRow parkingrow in ds.Tables[0].Rows
                         where Convert.ToString(parkingrow["PropertyAddress"].ToString().ToUpper()).Contains(Term.ToUpper())
                         select parkingrow["PropertyAddress"].ToString()
                                    ).ToList();



                }
                else
                {
                    //AvailableParkings.Add(new AvailableParkingAreaResult() { Success = false, ErrorMessage = Convert.ToString(SqlParms[7].Value) });
                    //return AvailableParkings;
                }

            }
            catch (Exception ex)
            {
                //AvailableParkings.Add(new AvailableParkingAreaResult() { Success = false, ErrorMessage = Convert.ToString(ex.Message.ToString()) });
                //return AvailableParkings;
            }

            return Locations;
        }
        #endregion

        public AvailableParkingAreaResult GetParkingDetails(string parkId)
        {
            AvailableParkingAreaResult objAvailableParkingAreaResult = new AvailableParkingAreaResult();

            DataSet ds = null;

            SqlParameter[] SqlParms = new SqlParameter[2];

            SqlParms[0] = new SqlParameter("@ParingId", SqlDbType.Int);
            SqlParms[0].Direction = ParameterDirection.Input;
            SqlParms[0].Value = parkId;

            SqlParms[1] = new SqlParameter("@Error", SqlDbType.VarChar, 150);
            SqlParms[1].Direction = ParameterDirection.Output;

            ds = SqlHelper.ExecuteDataset(connectionString, CommandType.StoredProcedure, "GetParkingDetails", SqlParms);
            objAvailableParkingAreaResult = (from DataRow parkingrow in ds.Tables[0].Rows
                                             select new AvailableParkingAreaResult
                                             {
                                                 Description = Convert.ToString(parkingrow["Description"]),
                                                 Rating = (parkingrow["ParkingRating"] == System.DBNull.Value ? 0 : Convert.ToInt32(parkingrow["ParkingRating"]))

                                             }).FirstOrDefault();
            return objAvailableParkingAreaResult;
        }
    }
}
