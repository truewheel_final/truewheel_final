﻿using Microsoft.ApplicationBlocks.Data;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using TrueWheels.BEL;

namespace TrueWheels.DAL
{
    public class VisitorHistoryDAL
    {
        SqlCommand cmd;
        SqlConnection con;
        SqlDataAdapter da;
        DataTable dt;
        DataSet ds;
        private string connectionString = string.Empty;

        public VisitorHistoryDAL()
        {
            connectionString = ConfigurationManager.ConnectionStrings["conStr"].ConnectionString;
            con = new SqlConnection(connectionString);
        }

        public List<VisitorHistoryBEL> VisitorHistory()
        {
            VisitorHistoryBEL visitorHistoryBEL = new VisitorHistoryBEL();
            List<VisitorHistoryBEL> visitorHistoryList = getRecord(1);
            return visitorHistoryList;
        }
        public VisitorHistoryBEL VisitorHistory(int visitornumber)
        {

            VisitorHistoryBEL visitorHistoryBEL = new VisitorHistoryBEL();
            visitorHistoryBEL.VisitorNumber = visitornumber;
            List<VisitorHistoryBEL> visitorHistoryList = getRecord(2, visitorHistoryBEL);
            return visitorHistoryList[0];
        }
        public List<VisitorHistoryBEL> VisitorHistory(string visitorid)
        {
            VisitorHistoryBEL visitorHistoryBEL = new VisitorHistoryBEL();
            visitorHistoryBEL.VisitorID = visitorid;
            List<VisitorHistoryBEL> visitorHistoryList = getRecord(3, visitorHistoryBEL);
            return visitorHistoryList;
        }

        public List<VisitorHistoryBEL> VisitorHistory(string fromdatetime, string todatetime)
        {
            VisitorHistoryBEL visitorHistoryBEL = new VisitorHistoryBEL();
            visitorHistoryBEL.FromDate = fromdatetime;
            visitorHistoryBEL.ToDate = todatetime;
            List<VisitorHistoryBEL> visitorHistoryList = getRecord(4, visitorHistoryBEL);
            return visitorHistoryList;
        }

        //public List<VisitorHistoryBEL> VisitorHistory(string source, string destination)
        //{
        //    VisitorHistoryBEL visitorHistoryBEL = new VisitorHistoryBEL();
        //    visitorHistoryBEL.SourceAddress = source;
        //    visitorHistoryBEL.DestinationAddress = destination;
        //    List<VisitorHistoryBEL> visitorHistoryList = getRecord(4, visitorHistoryBEL);
        //    return visitorHistoryList;
        //}

        public List<VisitorHistoryBEL> VisitorHistory(string source, string destination, string fromdatetime, string todatetime)
        {
            VisitorHistoryBEL visitorHistoryBEL = new VisitorHistoryBEL();
            visitorHistoryBEL.SourceAddress = source;
            visitorHistoryBEL.DestinationAddress = destination;
            visitorHistoryBEL.FromDate = fromdatetime;
            visitorHistoryBEL.ToDate = todatetime;
            List<VisitorHistoryBEL> visitorHistoryList = getRecord(5, visitorHistoryBEL);
            return visitorHistoryList;
        }
        public bool VisitorHistory(VisitorHistoryBEL visitorHistory)
        {
            Boolean flag = false;
            try
            {
               
                SqlParameter[] param = new SqlParameter[10];
                #region param 1
                param[0] = new SqlParameter("@Visitor_id", SqlDbType.NVarChar, 300);
                param[0].Direction = ParameterDirection.Input;
                param[0].Value = visitorHistory.VisitorID;
                #endregion
                #region param 1
                param[1] = new SqlParameter("@Visitor_email_id", SqlDbType.NVarChar, 300);
                param[1].Direction = ParameterDirection.Input;
                param[1].Value = visitorHistory.VisitorMailID;
                #endregion
                #region param 1
                param[2] = new SqlParameter("@Source_Address", SqlDbType.NVarChar, 300);
                param[2].Direction = ParameterDirection.Input;
                param[2].Value = visitorHistory.SourceAddress;
                #endregion
                #region param 1
                param[3] = new SqlParameter("@Destination_Address", SqlDbType.NVarChar, 300);
                param[3].Direction = ParameterDirection.Input;
                param[3].Value = visitorHistory.DestinationAddress;
                #endregion
                #region param 1
                //param[4] = new SqlParameter("@DatetimeStamp", SqlDbType.NVarChar, 300);
                //param[4].Direction = ParameterDirection.Input;
                //param[4].Value = visitorHistory.FromDate;
                #endregion
                //#region param 1
                //param[5] = new SqlParameter("@To_datetime", SqlDbType.NVarChar, 300);
                //param[5].Direction = ParameterDirection.Input;
                //param[5].Value = visitorHistory.ToDate;
                //#endregion
                #region param 1
                param[6] = new SqlParameter("@Error", SqlDbType.NVarChar, 300);
                param[6].Direction = ParameterDirection.Output;
                //param[0].Value = visitorHistory.VisitorID;
                #endregion
                #region param 1
                param[7] = new SqlParameter("@VisitorNumber", SqlDbType.NVarChar, 300);
                param[7].Direction = ParameterDirection.Output;
                //param[0].Value = visitorHistory.VisitorID;
                #endregion
                SqlHelper.ExecuteDataset(connectionString, CommandType.StoredProcedure, "Insert_VisitorHistory", param);
                #region 1
                //con = new SqlConnection(connectionString);
                //cmd = new SqlCommand("Insert_VisitorHistory", con);
                //cmd.CommandType = CommandType.StoredProcedure;
                //cmd.Parameters.AddWithValue("@Visitor_id", visitorHistory.VisitorID);
                //cmd.Parameters.AddWithValue("@Visitor_email_id", visitorHistory.VisitorMailID);
                //cmd.Parameters.AddWithValue("@Source_Address", visitorHistory.SourceAddress);
                //cmd.Parameters.AddWithValue("@Destination_address", visitorHistory.DestinationAddress);
                //cmd.Parameters.AddWithValue("@From_datetime", visitorHistory.FromDate);
                //cmd.Parameters.AddWithValue("@To_datetime", visitorHistory.ToDate);
                ////cmd.Parameters.AddWithValue("@DatetimeStamp", visitorHistory.VisitorID);

                //cmd.Parameters.Add("@Error", SqlDbType.VarChar, 500).Direction = ParameterDirection.Output;
                ////cmd.Parameters["@Error"].Direction = ParameterDirection.Output;
                //cmd.Parameters.Add("@VisitorNumber", SqlDbType.VarChar, 500).Direction = ParameterDirection.Output;
                ////cmd.Parameters["@VisitorNumber"].Direction = ParameterDirection.Output;
                //try
                //{
                //    con.Open();
                //    int i = cmd.ExecuteNonQuery();
                //    if (i > 0)
                //    {
                //        flag = true;
                //    }
                //}
                //catch (Exception ex)
                //{

                //}
                //finally
                //{
                //    con.Close();
                //}
                #endregion
                if (!(param[6].Value.ToString() == ""))
                {
                    flag = true;
                }
            }
            catch (Exception ex)
            {
                ErrorLog.Log(this.GetType().Name, MethodBase.GetCurrentMethod().Name, ex.Message.ToString(), HttpContext.Current.Session["userPhoneNo/Email"] != null ? HttpContext.Current.Session["userPhoneNo/Email"].ToString() : "");
                throw ex;
            }
            return flag;
        }
        
        private List<VisitorHistoryBEL> getRecord(int i, VisitorHistoryBEL visitorHistoryBEL = null)
        {
            List<VisitorHistoryBEL> visitorHistoryList = null;
            visitorHistoryList = new List<VisitorHistoryBEL>();
            SqlParameter[] param = null;
            dt = new DataTable();
            ds = new DataSet();
            cmd = new SqlCommand("sp_GetVisitorHistory", con);
            #region 1
            if (i == 1)
            {
                param = new SqlParameter[1];
                param[0] = new SqlParameter("@methodcode", SqlDbType.Int);
                param[0].Direction = ParameterDirection.Input;
                param[0].Value = 1;
            }
            #endregion
            #region 2
            if (i == 2)
            {
                param = new SqlParameter[2];
                param[0] = new SqlParameter("@methodcode", SqlDbType.Int);
                param[0].Direction = ParameterDirection.Input;
                param[0].Value = 1;

                param[1] = new SqlParameter("@Visitor_number", SqlDbType.NVarChar, 100);
                param[1].Direction = ParameterDirection.Input;
                param[1].Value = visitorHistoryBEL.VisitorNumber;

                //visitorHistoryList = new List<VisitorHistoryBEL>();
                //dt = new DataTable();
                //cmd = new SqlCommand("sp_GetVisitorHistory", con);
                //cmd.Parameters.AddWithValue("@methodcode", 2);
                //cmd.Parameters.AddWithValue("@Visitor_number", visitorHistoryBEL.VisitorNumber);
                //da = new SqlDataAdapter(cmd);
                //da.Fill(dt);
                //visitorHistoryList = convertToList(dt);
            }
            #endregion
            #region 3
            if (i == 3)
            {
                param = new SqlParameter[2];
                param[0] = new SqlParameter("@methodcode", SqlDbType.Int);
                param[0].Direction = ParameterDirection.Input;
                param[0].Value = 1;

                param[1] = new SqlParameter("@Visitor_id", SqlDbType.NVarChar, 100);
                param[1].Direction = ParameterDirection.Input;
                param[1].Value = visitorHistoryBEL.VisitorID;

                //visitorHistoryList = new List<VisitorHistoryBEL>();
                //dt = new DataTable();
                //cmd = new SqlCommand("sp_GetVisitorHistory", con);
                //cmd.Parameters.AddWithValue("@methodcode", 3);
                //cmd.Parameters.AddWithValue("@Visitor_number", visitorHistoryBEL.VisitorNumber);
                //da = new SqlDataAdapter(cmd);
                //da.Fill(dt);
                //visitorHistoryList = convertToList(dt);
            }
            #endregion
            #region 4
            if (i == 4)
            {
                param = new SqlParameter[3];
                param[0] = new SqlParameter("@methodcode", SqlDbType.Int);
                param[0].Direction = ParameterDirection.Input;
                param[0].Value = 1;

                param[1] = new SqlParameter("@From_datetime", SqlDbType.NVarChar, 100);
                param[1].Direction = ParameterDirection.Input;
                param[1].Value = visitorHistoryBEL.FromDate;


                param[2] = new SqlParameter("@To_datetime", SqlDbType.NVarChar, 100);
                param[2].Direction = ParameterDirection.Input;
                param[2].Value = visitorHistoryBEL.ToDate;


                //visitorHistoryList = new List<VisitorHistoryBEL>();
                //dt = new DataTable();
                //cmd = new SqlCommand("sp_GetVisitorHistory", con);
                //cmd.Parameters.AddWithValue("@methodcode", 4);
                //cmd.Parameters.AddWithValue("@Visitor_id", visitorHistoryBEL.VisitorID);
                //da = new SqlDataAdapter(cmd);
                //da.Fill(dt);
                //visitorHistoryList = convertToList(dt);
            }
            #endregion
            #region 5
            if (i == 5)
            {
                param = new SqlParameter[5];

                param[0] = new SqlParameter("@methodcode", SqlDbType.Int);
                param[0].Direction = ParameterDirection.Input;
                param[0].Value = 1;

                param[1] = new SqlParameter("@Source_Address", SqlDbType.NVarChar, 300);
                param[1].Direction = ParameterDirection.Input;
                param[1].Value = visitorHistoryBEL.SourceAddress;


                param[2] = new SqlParameter("@Destination_address", SqlDbType.NVarChar, 300);
                param[2].Direction = ParameterDirection.Input;
                param[2].Value = visitorHistoryBEL.DestinationAddress;

                param[3] = new SqlParameter("@From_datetime", SqlDbType.NVarChar, 100);
                param[3].Direction = ParameterDirection.Input;
                param[3].Value = visitorHistoryBEL.FromDate;


                param[4] = new SqlParameter("@To_datetime", SqlDbType.NVarChar, 100);
                param[4].Direction = ParameterDirection.Input;
                param[4].Value = visitorHistoryBEL.ToDate;

                //visitorHistoryList = new List<VisitorHistoryBEL>();
                //dt = new DataTable();
                //cmd = new SqlCommand("sp_GetVisitorHistory", con);
                //cmd.Parameters.AddWithValue("@methodcode", 5);
                //cmd.Parameters.AddWithValue("@Visitor_", visitorHistoryBEL.VisitorNumber);
                //da = new SqlDataAdapter(cmd);
                //da.Fill(dt);
                //visitorHistoryList = convertToList(dt);
            }
            #endregion

            ds = SqlHelper.ExecuteDataset(connectionString, CommandType.StoredProcedure, "sp_GetVisitorHistory", param);
            visitorHistoryList = convertToList(ds.Tables[0]);
            return visitorHistoryList;
        }
        private List<VisitorHistoryBEL> convertToList(DataTable localDB)
        {
            int rowCount = localDB.Rows.Count;
            List<VisitorHistoryBEL> list = new List<VisitorHistoryBEL>();
            VisitorHistoryBEL visitorHistory; 
            if (rowCount > 0)
            {

                foreach(DataRow row in localDB.Rows)
                {
                    visitorHistory = new VisitorHistoryBEL();
                    visitorHistory.VisitorNumber = Convert.ToInt32(row["Visitor_number"].ToString());
                    visitorHistory.VisitorID = row["Visitor_id"].ToString();
                    visitorHistory.VisitorMailID = row["Visitor_email_id"].ToString();
                    visitorHistory.SourceAddress = row["Source_address"].ToString();
                    visitorHistory.DestinationAddress = row["Destination_address"].ToString();
                    visitorHistory.FromDate = row["From_datetime"].ToString();
                    visitorHistory.ToDate = row["To_datetime"].ToString();
                    list.Add(visitorHistory);
                }

            }
            return list;
        }

    }
}

